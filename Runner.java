package com.gmail.fcdtdk;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Runner {
    public static void main(String[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        @SuppressWarnings("InstantiationOfUtilityClass") TextContainer container = new TextContainer();
        Class<?> cls = container.getClass();

        SaveTo annSaveTo = cls.getAnnotation(SaveTo.class);

        Method[] methods = cls.getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(Saver.class)) method.invoke(container, annSaveTo.path());
        }
    }
}
